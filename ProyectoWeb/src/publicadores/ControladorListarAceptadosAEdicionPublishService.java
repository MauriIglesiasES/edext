/**
 * ControladorListarAceptadosAEdicionPublishService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package publicadores;

public interface ControladorListarAceptadosAEdicionPublishService extends javax.xml.rpc.Service {
    public java.lang.String getControladorListarAceptadosAEdicionPublishPortAddress();

    public publicadores.ControladorListarAceptadosAEdicionPublish getControladorListarAceptadosAEdicionPublishPort() throws javax.xml.rpc.ServiceException;

    public publicadores.ControladorListarAceptadosAEdicionPublish getControladorListarAceptadosAEdicionPublishPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
